<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Grimgerde PH</title>

        <link rel = "stylesheet" type = "text/css" href = "/css/styles.css" />

    </head>
    <body style="background-color: #393c45;">
    <div class='container'>
        <img class='resize_fit_center'
        src="/img/logo.jpg" />
    </div>
    </body>
</html>
